﻿# This is an uninstaller for XNA, assuming it was set up using the same script in this repository.
# However, it has not been thoroughly tested and may not do everything it's supposed to.
# By using this script you do so at your own risk. While I've made a good faith effort to produce a high quality
# script that does what it should and nothing more, it is provided AS-IS, with no guarantee for fitness for
# any particular purpose, and I hereby disclaim any responsibility for anything that may happen when you use this
# script.

Function RunUninstaller($path) {
    Write-Host ("Uninstalling " + $path);
    Start-Process -FilePath msiexec.exe -ArgumentList /uninstall, $path, /quiet -Wait;
}

Function RunInstaller($path) {
    if((Test-Path $path) -eq $True) {
        Write-Host ("Installing " + $path);
        Start-Process -FilePath msiexec.exe -ArgumentList /i, $path, /quiet -Wait;
    }
}

$InstallerLocation = "C:\Program Files (x86)\Microsoft XNA\XNA Game Studio\v4.0\";

Set-Location $InstallerLocation;

RunUninstaller("`"" + $InstallerLocation + "Setup\xnags_visualstudio.msi" + "`"");
RunUninstaller("`"" + $InstallerLocation + "Setup\xnags_platform_tools.msi" + "`"");
RunUninstaller("`"" + $InstallerLocation + "Setup\xnaliveproxy.msi" + "`"");
RunUninstaller("`"" + $InstallerLocation + "Redist\XNA FX Redist\xnafx40_redist.msi" + "`"");
RunUninstaller("`"" + $InstallerLocation + "Setup\XLiveRedist.msi" + "`"");
RunUninstaller("`"" + $InstallerLocation + "Setup\xnags_shared.msi" + "`"");

if((Test-Path "`"C:\Program Files (x86)\Microsoft Visual Studio 11.0\Common7\IDE\Extensions\Microsoft\XNA Game Studio 4.0`"") -eq $True) {
    Write-Host "Uninstalling from VS 2012 Pro.";
    Remove-Item -Recurse -Force "`"C:\Program Files (x86)\Microsoft Visual Studio 11.0\Common7\IDE\Extensions\Microsoft\XNA Game Studio 4.0`"";
    $pathToExe = "${Env:VS110COMNTOOLS}..\IDE\devenv.exe";
    Start-Process -FilePath $pathToExe -ArgumentList /setup -Wait;
}

if((Test-Path "`"C:\Program Files (x86)\Microsoft Visual Studio 11.0\Common7\IDE\WDExpressExtensions\Extensions\XNA Game Studio 4.0`"") -eq $True) {
    Write-Host "Uninstalling from VS 2012 Express.";
    Remove-Item -Recurse -Force "`"C:\Program Files (x86)\Microsoft Visual Studio 11.0\Common7\IDE\WDExpressExtensions\Extensions\XNA Game Studio 4.0`"";
    $pathToExe = "${Env:VS110COMNTOOLS}..\IDE\WDExpress.exe";
    Start-Process -FilePath $pathToExe -ArgumentList /setup -Wait;
}

if((Test-Path "`"C:\Program Files (x86)\Microsoft Visual Studio 12.0\Common7\IDE\Extensions\Microsoft\XNA Game Studio 4.0`"") -eq $True) {
    Write-Host "Uninstalling from VS 2013 Pro.";
    Remove-Item -Recurse -Force "`"C:\Program Files (x86)\Microsoft Visual Studio 12.0\Common7\IDE\Extensions\Microsoft\XNA Game Studio 4.0`"";
    $pathToExe = "${Env:VS110COMNTOOLS}..\IDE\devenv.exe";
    Start-Process -FilePath $pathToExe -ArgumentList /setup -Wait;
}

if((Test-Path "`"C:\Program Files (x86)\Microsoft Visual Studio 12.0\Common7\IDE\WDExpressExtensions\Extensions\XNA Game Studio 4.0`"") -eq $True) {
    Write-Host "Uninstalling from VS 2013 Express.";
    Remove-Item -Recurse -Force "`"C:\Program Files (x86)\Microsoft Visual Studio 12.0\Common7\IDE\WDExpressExtensions\Extensions\XNA Game Studio 4.0`"";
    $pathToExe = "${Env:VS110COMNTOOLS}..\IDE\WDExpress.exe";
    Start-Process -FilePath $pathToExe -ArgumentList /setup -Wait;
	
	
if((Test-Path "`"C:\Program Files (x86)\Microsoft Visual Studio 14.0\Common7\IDE\Extensions\Microsoft\XNA Game Studio 4.0`"") -eq $True) {
    Write-Host "Uninstalling from VS 2013 Pro.";
    Remove-Item -Recurse -Force "`"C:\Program Files (x86)\Microsoft Visual Studio 14.0\Common7\IDE\Extensions\Microsoft\XNA Game Studio 4.0`"";
    $pathToExe = "${Env:VS110COMNTOOLS}..\IDE\devenv.exe";
    Start-Process -FilePath $pathToExe -ArgumentList /setup -Wait;
}